import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// eslint-disable-next-line
import './registerServiceWorker';
// eslint-disable-next-line
import Vuetify from 'vuetify';
// eslint-disable-next-line
import 'vuetify/dist/vuetify.min.css';
// eslint-disable-next-line
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.green.lighten3,
    secondary: colors.teal.lighten3,
    accent: colors.lightBlue.darken3,
    error: colors.red.base,
    warning: colors.yellow.base,
    info: colors.blue.base,
    success: colors.green.base,
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
